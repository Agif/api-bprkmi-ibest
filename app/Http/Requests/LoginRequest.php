<?php

namespace App\Http\Requests;

use Pearl\RequestValidate\RequestAbstract;

class LoginRequest extends RequestAbstract
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email'
            // 'password' => 'required|min:5'
        ];
    }

    public function messages(): array
    {
        return [
            'email.required'    => 'Email tidak boleh kosong',
            'email.email'       => 'Email tidak valid'
            // 'password.required' => 'Password harus diisi',
            // 'password.min'      => 'Password minimal 5 karakter'
        ];
    }
}
