<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'kmi_user';
    protected $primaryKey = 'id';
    protected $fillable = [
        'username', 'fullname', 'nasabah_id', 'kode_kantor', 'link', 'alamat', 'no_ktp', 'no_ktp_verified', 'no_hp', 'no_hp_verified', 'kode_otp', 'imei', 'email', 'email_verified', 'user_verified', 'tgl_verifikasi', 'fcm_token', 'blokir', 'blokir_date', 'create_date', 'last_update', 'avatar_user', 'approve_by', 'flag_del'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'fcm_token'
    ];

    public $timestamps = false;
}
