<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Mail;

class Controller extends BaseController
{
    function group_by($key, $array) {
	    $result = array();
	    $q = 0;
	    foreach($array as $keys =>  $val) {
	        if(array_key_exists($key, $val)){
	            $result[$val[$key]]['tanggal_transaksi'] = $val[$key];
	            $result[$val[$key]]['data'][] = $val;
	        }else{
	            $result[""][] = $val;
	        }
	    }

	    return $result;
	}

    function arrSearch($array) {
        $dup_array = array_unique($array);
        $del_array = array_search(null, $dup_array);
        unset($dup_array[$del_array]);
        return $dup_array;
    }

    function mail($view, $from, $to, $cc=false, $subject, $msg, $files=false){
        $strTo   = str_replace(' ','',$to);
        $receipt = explode(',',$strTo);

        if ($cc) {
            $strToCC = str_replace(' ','',$cc);
            $ccTo    = explode(',',$strToCC);
        }else{
            $ccTo = $cc;
        }

        if ($files) {
            $newFiles = $this->arrSearch($files);
        }else{
            $newFiles = $files;
        }

        $point = str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n"),"<br/>", $msg);

        $mail = Mail::send(['html' => $view], $point, function($message) use($from, $subject, $receipt, $ccTo, $newFiles){
            $message->to($receipt);

            if ($ccTo) {
                $message->cc($ccTo);
            }

            if ($newFiles) {
                for ($x = 1; $x <= count($newFiles); $x++) {
                    $message->attach($newFiles[$x]->getRealPath(),
                    [
                        'as' => $newFiles[$x]->getClientOriginalName(),
                        'mime' => $newFiles[$x]->getClientMimeType(),
                    ]);
                }
            }

            if ($from) {
                $message->from($from);
            }

            $message->subject($subject);
        });

        return $mail;
    }
}
