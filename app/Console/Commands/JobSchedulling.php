<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class JobSchedulling extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // protected $signature = 'command:name';
    protected $signature = 'actions:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Server start to schedulling manage flg_sent in DB ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $query = DB::update('UPDATE sys_pesan SET flg_sent = 1 WHERE `id`=?', [result[0].id]);
    }
}
