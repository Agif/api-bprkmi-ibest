<?php

namespace App\Http\Requests;

use Pearl\RequestValidate\RequestAbstract;

class updateRequest extends RequestAbstract
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    public function rules(): array
    {
        return [
            // "username"          => "unique:kmi_user",
            // "fullname"          => "",
            "nasabah_id"        => "numeric",
            // "alamat"            => "",
            "no_ktp"            => "numeric",
            "no_hp"             => "numeric|digits_between:10,13",
            "no_hp_verified"    => "numeric|max:1",
            "kode_otp"          => "numeric",
            "imei"              => "numeric",
            "email"             => "email",
            "email_verified"    => "numeric|max:1",
            "user_verified"     => "numeric|max:1",
            // "password"          => "",
            "avatar_user"       => "mimes:jpeg,jpg,png,gif|max:2000"
        ];
    }

    public function messages(): array
    {
        return [
            // "username.required"       => "Username tidak boleh kosong",
            // "username.unique"         => "Username sudah ada yang menggunakan",

            // "fullname.required"       => "Fullname tidak boleh kosong",

            // "nasabah_id.required"     => "Nasabah_id tidak boleh kosong",
            "nasabah_id.numeric"      => "Nasabah_id harus berupa angka",
            // "nasabah_id.unique"       => "nasabah_id sudah ada yang menggunakan",

            // "kode_kantor.required"    => "kode_kantor tidak boleh kosong",

            // "alamat.required"                  => "Alamat tidak boleh kosong",

            // "no_ktp.required"         => "no_ktp tidak boleh kosong",
            "no_ktp.numeric"          => "no_ktp harus berupa angka",
            // "no_ktp.unique"           => "no_ktp sudah ada yang menggunakan",

            // "no_hp.required"          => "no_hp tidak boleh kosong",
            "no_hp.numeric"           => "no_hp harus berupa angka",
            // "no_hp.unique"            => "no_hp sudah ada yang menggunakan",
            "no_hp.digits_between"    => "no_hp harus berjumlah antara 10 - 13 digit angka",

            // "no_hp_verified.required" => "no_hp_verified tidak boleh kosong",
            "no_hp_verified.numeric"  => "no_hp_verified harus berupa angka",
            "no_hp_verified.max"      => "no_hp_verified maksimal 1 digit angka",

            // "kode_otp.required"       => "kode_otp tidak boleh kosong",
            "kode_otp.numeric"        => "kode_otp harus berupa angka",

            // "imei.required"           => "imei tidak boleh kosong",
            "imei.numeric"            => "imei harus berupa angka",

            // "email.required"          => "Email tidak boleh kosong",
            "email.email"             => "Format email tidak benar",
            // "email.unique"            => "Email sudah ada yang menggunakan",

            // "email_verified.required" => "email_verified tidak boleh kosong",
            "email_verified.numeric"  => "email_verified harus berupa angka",
            "email_verified.max"      => "email_verified maksimal 1 digit angka",

            // "user_verified.required"  => "user_verified tidak boleh kosong",
            "user_verified.numeric"   => "user_verified harus berupa angka",
            "user_verified.max"       => "user_verified maksimal 1 digit angka",
            // "password.required"       => "password tidak boleh kosong",
            "avatar_user.mimes" => "avatar_user Harus berformat gambar dengan ekstensi jpeg,jpg,png atau gif",
            "avatar_user.max" => "avatar_user maksimal berukuran 2 Mb",
        ];
    }
}
