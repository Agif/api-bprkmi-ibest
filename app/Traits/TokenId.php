<?php

namespace App\Traits;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Firebase\JWT\JWT;

trait TokenId {

    public function GTI(Request $req){
        $token = $req->header('Authorization');

        if(preg_match("/JWT /", $token)){

            $strToken = str_replace('JWT ','', $token);

            $credentials = JWT::decode($strToken, env('JWT_SECRET'), ['HS256']);
                // print_r($decoded);

            $data = $credentials->id;

            return $data;
        }else{
            return false;
        }
    }

    public function nasID(Request $req){
        $token = $req->header('Authorization');

        if(preg_match("/JWT /", $token)){

            $strToken = str_replace('JWT ','', $token);

            $credentials = JWT::decode($strToken, env('JWT_SECRET'), ['HS256']);
                // print_r($decoded);

            $data = $credentials->nasabah_id;

            return $data;
        }else{
            return false;
        }
    }
}
