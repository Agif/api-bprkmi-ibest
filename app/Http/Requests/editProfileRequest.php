<?php

namespace App\Http\Requests;

use Pearl\RequestValidate\RequestAbstract;

class editProfileRequest extends RequestAbstract
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "no_hp"    => "numeric|digits_between:10,13",
            "no_ktp"   => "numeric|digits_between:15,16"
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            "no_hp.numeric"         => "No Hp harus berupa angka",
            "no_hp.digits_between"  => "No Hp harus berjumlah diantara 10 - 13 digit",
            "no_ktp.numeric"        => "No KTP harus berupa angka",
            "no_ktp.digits_between" => "No Hp harus berjumlah diantara 15 - 16 digit"
        ];
    }
}
