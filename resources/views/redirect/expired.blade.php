<!DOCTYPE html>
<html>
<head>
	<title>BPR Kredit Mandiri Indonesia</title>
	<link rel="shortcut icon" href="Logo(2).png" />
	<style type="text/css">
    body {
      background-color: black;
      background-image: url("#");
      background-size:  contain;
      background-repeat: no-repeat;
       background-position: center;
       background-attachment: fixed;
      min-width: 640px;
    }
    body div.scene {
      background: transparent;
      width: 640px;
      height: 480px;
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      margin: auto;
      overflow: hidden;
      -webkit-filter: url("#goo");
              filter: url("#goo");
    }
    body div.scene div.pill {
      background: #FF0000;
      width: 130px;
      height: 130px;
      position: absolute;
      top: 150px;
      left: 0;
      right: 0;
      margin: auto;
      -webkit-animation: pill_wiggle 4s linear normal;
              animation: pill_wiggle 4s linear normal;
    }
    body div.scene div.drop {
      background: #FF0000;
      width: 60px;
      height: 60px;
      position: absolute;
      top: 185px;
      left: 0;
      right: 0;
      margin: auto;
      border-radius: 50%;
      -webkit-animation: drop_wiggle 4s ease-in normal;
              animation: drop_wiggle 4s ease-in normal;
    }
    body div.scene div.drop_trail {
      background:#FF0000;
      width: 40px;
      height: 40px;
      position: absolute;
      top: 195px;
      left: 0;
      right: 0;
      margin: auto;
      -webkit-animation: drop_trail_wiggle 4s ease-out normal;
              animation: drop_trail_wiggle 4s ease-out normal;
    }
    body div.logo_contraint {
      background: transparent;
      width: 640px;
      height: 480px;
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      margin: auto;
      overflow: hidden;
    }
    body div.logo_contraint div.logo {
      width: 130px;
      height: 130px;
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      margin: auto;
      -webkit-animation: logo_anim 4s linear normal;
              animation: logo_anim 4s linear normal;
    }
    body div.logo_contraint div.logo img {
      margin-top: -13px;
    }
    body div.logo_contraint div#url {
      position: absolute;
      left: 0;
      right: 0;
      margin: auto;
      margin-top: 300px;
      text-align: center;
    }
    body div.logo_contraint div#url a {
      color: white;
      font-family: 'Dosis', sans-serif;
      font-weight: bold;
      text-decoration: none;
      padding-bottom: 5px;
      border-bottom: 2px solid #FF0000;
      cursor: pointer;
      -webkit-animation: url_animation 4s linear normal;
              animation: url_animation 4s linear normal;
    }
    body div.logo_contraint div#url a:hover {
      color: #FFD700;
      cursor: pointer;
    }

    @-webkit-keyframes pill_wiggle {
      0% {
        left: 600px;
        -webkit-transform: rotateZ(0deg);
                transform: rotateZ(0deg);
      }
      15% {
        left: -100px;
        -webkit-transform: rotateZ(-35deg);
                transform: rotateZ(-35deg);
        -webkit-transform-origin: 0% 100%;
                transform-origin: 0% 100%;
      }
      20% {
        left: 100px;
        -webkit-transform: rotateZ(20deg);
                transform: rotateZ(20deg);
        -webkit-transform-origin: 100% 100%;
                transform-origin: 100% 100%;
      }
      25% {
        left: 0;
        -webkit-transform: rotateZ(-10deg);
                transform: rotateZ(-10deg);
        -webkit-transform-origin: 0% 100%;
                transform-origin: 0% 100%;
      }
      30% {
        left: 0;
        -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
        -webkit-transform-origin: 0% 100%;
                transform-origin: 0% 100%;
      }
      40% {
        left: 30px;
        -webkit-transform: rotate(25deg);
                transform: rotate(25deg);
        -webkit-transform-origin: 100% 100%;
                transform-origin: 100% 100%;
      }
      45% {
        left: -5px;
        -webkit-transform: rotate(-10deg);
                transform: rotate(-10deg);
        -webkit-transform-origin: 0% 100%;
                transform-origin: 0% 100%;
      }
      50% {
        left: 0;
        -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
        -webkit-transform-origin: 100% 100%;
                transform-origin: 100% 100%;
      }
      60% {
        left: 0;
        -webkit-transform: rotate(-10deg);
                transform: rotate(-10deg);
        -webkit-transform-origin: 0% 100%;
                transform-origin: 0% 100%;
      }
      65% {
        left: 0;
        -webkit-transform: rotateZ(0deg);
                transform: rotateZ(0deg);
        -webkit-transform-origin: 0% 100%;
                transform-origin: 0% 100%;
      }
    }

    @keyframes pill_wiggle {
      0% {
        left: 600px;
        -webkit-transform: rotateZ(0deg);
                transform: rotateZ(0deg);
      }
      15% {
        left: -100px;
        -webkit-transform: rotateZ(-35deg);
                transform: rotateZ(-35deg);
        -webkit-transform-origin: 0% 100%;
                transform-origin: 0% 100%;
      }
      20% {
        left: 100px;
        -webkit-transform: rotateZ(20deg);
                transform: rotateZ(20deg);
        -webkit-transform-origin: 100% 100%;
                transform-origin: 100% 100%;
      }
      25% {
        left: 0;
        -webkit-transform: rotateZ(-10deg);
                transform: rotateZ(-10deg);
        -webkit-transform-origin: 0% 100%;
                transform-origin: 0% 100%;
      }
      30% {
        left: 0;
        -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
        -webkit-transform-origin: 0% 100%;
                transform-origin: 0% 100%;
      }
      40% {
        left: 30px;
        -webkit-transform: rotate(25deg);
                transform: rotate(25deg);
        -webkit-transform-origin: 100% 100%;
                transform-origin: 100% 100%;
      }
      45% {
        left: -5px;
        -webkit-transform: rotate(-10deg);
                transform: rotate(-10deg);
        -webkit-transform-origin: 0% 100%;
                transform-origin: 0% 100%;
      }
      50% {
        left: 0;
        -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
        -webkit-transform-origin: 100% 100%;
                transform-origin: 100% 100%;
      }
      60% {
        left: 0;
        -webkit-transform: rotate(-10deg);
                transform: rotate(-10deg);
        -webkit-transform-origin: 0% 100%;
                transform-origin: 0% 100%;
      }
      65% {
        left: 0;
        -webkit-transform: rotateZ(0deg);
                transform: rotateZ(0deg);
        -webkit-transform-origin: 0% 100%;
                transform-origin: 0% 100%;
      }
    }
    @-webkit-keyframes drop_wiggle {
      0% {
        left: 600px;
      }
      10% {
        left: -50px;
      }
      20% {
        left: -400px;
      }
      21% {
        left: -400px;
      }
      40% {
        left: 500px;
      }
      60% {
        left: 0;
      }
    }
    @keyframes drop_wiggle {
      0% {
        left: 600px;
      }
      10% {
        left: -50px;
      }
      20% {
        left: -400px;
      }
      21% {
        left: -400px;
      }
      40% {
        left: 500px;
      }
      60% {
        left: 0;
      }
    }
    @-webkit-keyframes logo_anim {
      0% {
        opacity: 0;
      }
      60% {
        opacity: 0;
      }
      100% {
        opacity: 1;
      }
    }
    @keyframes logo_anim {
      0% {
        opacity: 0;
      }
      60% {
        opacity: 0;
      }
      100% {
        opacity: 1;
      }
    }
    @-webkit-keyframes drop_trail_wiggle {
      0% {
        left: 600px;
        background: #FF0000;
      }
      10% {
        left: -50px;
        background: #FF0000;
      }
      20% {
        background: #FF0000;
        left: -400px;
      }
      21% {
        left: -400px;
        background: #FF0000;
      }
      40% {
        background: #FF0000;
        left: 500px;
      }
      60% {
        background: #FF0000;
        left: 0;
      }
    }
    @keyframes drop_trail_wiggle {
      0% {
        left: 600px;
        background: #FF0000;
      }
      10% {
        left: -50px;
        background: #FF0000;
      }
      20% {
        background: #FF0000;
        left: -400px;
      }
      21% {
        left: -400px;
        background: #FF0000;
      }
      40% {
        background: #FF0000;
        left: 500px;
      }
      60% {
        background: #FF0000;
        left: 0;
      }
    }
    @-webkit-keyframes url_animation {
      0% {
        padding-bottom: 30px;
        opacity: 0;
      }
      90% {
        padding-bottom: 30px;
        opacity: 0;
      }
      100% {
        padding-bottom: 5px;
        opacity: 1;
      }
    }
    @keyframes url_animation {
      0% {
        padding-bottom: 30px;
        opacity: 0;
      }
      90% {
        padding-bottom: 30px;
        opacity: 0;
      }
      100% {
        padding-bottom: 5px;
        opacity: 1;
      }
    }
	</style>
</head>
<body>

<div class='scene'>
  <div class='pill'></div>
  <div class='drop'></div>
  <div class='drop_trail'></div>
</div>

<div class="logo_contraint">
  <div class='logo'>
    {{-- <img draggable="false" src="http://103.234.254.186/download/logo(2).png" alt="kredit mandiri indonesia" width="130" height="110"> --}}
  	<img draggable="false" src="http://103.234.254.186/download/images/bpr-logo.png" alt="kredit mandiri indonesia" width="130" height="110">
  </div>
  <div id="url">
    <a href="http://kreditmandiri.co.id/">LINK EXPIRED</a>
  </div>
</div>
<svg xmlns="http://www.w3.org/2000/svg" version="1.1">
  <defs>
    <filter id="goo">
      <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
      <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
      <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
    </filter>
  </defs>
</svg>
</body>
</html>
