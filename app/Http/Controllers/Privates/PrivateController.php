<?php

namespace App\Http\Controllers\Privates;

use App\Http\Requests\uploadImageRequest;
use App\Http\Requests\editProfileRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\OTP;
use Illuminate\Http\Request;
use App\Traits\TokenId;
use App\Http\Requests;
use Firebase\JWT\JWT;
use App\User;
use DB;


class PrivateController extends Controller
{
    use TokenId;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req)
    {
        //
    }

    public function simulasiKredit($plafon, $tenor){
        if(preg_match("/[[:digit:]]/", $plafon) && preg_match("/[[:digit:]]/", $tenor)){
            try {
                $equals = $plafon / $tenor; //get pokok kredit

                $sukubunga = $plafon * 1.7 / 100 ; //get sukubunga

                $pokokKredit = $equals + $sukubunga; //get pokok kredit + sukubunga = angsuran

                $result = json_decode(json_encode($pokokKredit, true));
            }catch(\Exception $e){
                return response()->json([
                    'status'=>'error',
                    'message'=>$e
                ], 412);
            }

            return response([
                'result' => (int)$pokokKredit
            ], 200);
        }else{
             return response()->json([
                'status'=>'Bad Request',
                'message'=> 'parameter harus berupa angka'
         ], 412);
        }
    }

    public function notif(Request $req){
        $user_id = $this->GTI($req);
        try {
            $query = DB::select("SELECT id, user_id, title, link, short_content, content, `show`, CONCAT(DATE(create_date), 'T',TIME(create_date),'.000Z') AS create_date, CONCAT(DATE(expired_date), 'T',TIME(expired_date),'.000Z') AS expired_date, CONCAT(DATE(last_update), 'T',TIME(last_update),'.000Z') AS last_update FROM kmi_notification WHERE `show` = 1 AND user_id=?", [$user_id]);

            $data = array();
            $i=0;
            foreach ($query as $key => $val) {
                $data[$i] = [
                    "id" => (int) $val->id,
                    "user_id" => (int) $val->user_id,
                    "title" => $val->title,
                    "link" => $val->link,
                    "short_content" => $val->short_content,
                    "content" => $val->content,
                    "show" => (int) $val->show,
                    "create_date" => $val->create_date,
                    "expired_date" => $val->expired_date,
                    "last_update" => $val->last_update
                ];
                $i++;
            }


        }catch(\Exception $e){
			return response()->json([
	       		'status'=>'error',
	       		'message'=>$e
			], 412);
		}

        return response(
			$data
		, 200);
    }

    public function tracking_order($no_aplikasi){
        if(preg_match("/[[:digit:]]/", $no_aplikasi)){
            $path = realpath('json/dummytracking.json');

            try {
                $json = json_decode(file_get_contents($path), true);
            }catch(\Exception $e){
                return response()->json([
                    'status'=>'error',
                    'message'=>$e
                ], 412);
            }

            return response($json, 200);
        }else{
            return response()->json([
                'status'=>'error',
                'message'=> 'No Aplikasi harus berupa angka'
         ], 412);
        }
    }

    public function countNotif(Request $req){
        $user_id = $this->GTI($req);

        try {

            $query = DB::select("SELECT COUNT(*) AS count_inbox FROM kmi_notification WHERE `show` = 1 AND user_id=?",[$user_id]);

            $result = $query[0]->count_inbox;

        }catch(\Exception $e){
			return response()->json([
	       		'message'=> $e
			], 400);
		}

        return response([
			'count_inbox' => $result
		], 200);
    }

    public function updateNotif(Request $req, $id_notification){

        $user_id = $this->GTI($req);

        $query = DB::update("UPDATE kmi_notification SET `show` =0 WHERE `id`=? AND `user_id`=?",[$id_notification, $user_id]);

        return response([
            "message" => "update notification success"
        ], 201);
    }

    public function flg_oto(Request $req, $id_otorisasi, $flg_otorisasi){

        $flagotor = $flg_otorisasi;

        $id = $id_otorisasi;

        $user_id = $this->GTI($req);

        $querySearch = DB::table('flg_otorisasi')->where('user_id', 'like', "%$user_id%")->get();

        if(preg_match("/[[:digit:]]/", $id)){
            if(preg_match("/[[:digit:]]/", $flagotor)){
                if(strlen($flagotor) <= 1 ){
                    if($querySearch != '[]'){

                        $query = DB::update("UPDATE flg_otorisasi SET `otorisasi`=?, `waktu_otorisasi`=NOW() WHERE `id`=? AND user_id=?",[$flagotor, $id, $user_id]);

                        return response([
                            "message" => "update notification success"
                        ], 201);
                    }else{
                        return response([
                            'status' => 'Bad Request',
                            'message' => 'ID User anda dan ID Otorisasi tidak berelasi'
                        ], 400);
                    }
                }else{
                    return response([
                        'status' => 'Bad Request',
                        'message' => 'Nilai flg_otorisasi yang tepat hanya 1 digit Misal: 1'
                    ], 400);
                }
            }else{
                return response([
                    'status' => 'Bad Request',
                    'message' => 'Nilai flg_otorisasi Harus berupa angka'
                ], 400);
            }
        }else{
            return response([
                'status' => 'Bad Request',
                'message' => 'ID flg_otorisasi Harus berupa angka'
            ], 400);
        }
    }

    public function get_data_user(Request $req){
        $user_id = $this->GTI($req);

        $query = DB::table('kmi_user')->where('id', $user_id)->first();

        if ($query) {
            $Insert = DB::insert("INSERT INTO `kmi_log` SET user_id=?",[$user_id]);

            $data = array(
                "id"                => (int) $query->id,
                "username"          => $query->username,
                "fullname"          => $query->fullname,
                "nasabah_id"        => $query->nasabah_id,
                "kode_kantor"       => $query->kode_kantor,
                "link"              => $query->link,
                "alamat"            => $query->alamat,
                "no_ktp"            => $query->no_ktp,
                "no_ktp_verified"   => $query->no_ktp_verified,
                "no_hp"             => $query->no_hp,
                "no_hp_verified"    => (int) $query->no_hp_verified,
                "kode_otp"          => (int) $query->kode_otp,
                "imei"              => $query->imei,
                "email"             => $query->email,
                "email_verified"    => (int) $query->email_verified,
                "email_verified_id" => $query->email_verified_id,
                "user_verified"     => (int) $query->user_verified,
                "tgl_verifikasi"    => $query->tgl_verifikasi,
                "password"          => $query->password,
                "fcm_token"         => $query->fcm_token,
                "blokir"            => (int) $query->blokir,
                "blokir_date"       => $query->blokir_date,
                "create_date"       => $query->create_date,
                "last_update"       => $query->last_update,
                "avatar_user"       => $query->avatar_user,
                "approve_by"        => $query->approve_by,
                "flag_del"          => $query->flag_del
            );

            return response(
                $data
            , 201);
        }else{
            return response()->json([
                'message'=> $e
            ], 400);
        }
    }

    public function simpanan(Request $req){
        $user_id = $this->GTI($req);
        $user = User::where('id', $user_id)->first();
        $nasabah_id = $user->nasabah_id;

        try {

            DB::statement(DB::raw("SET @pv_nasabah_id = '$nasabah_id' "));
            $queryTabungan = DB::select(
                DB::raw(
                    "SELECT nasabah_id, nama_nasabah, no_rekening, FLOOR(ROUND(saldo_akhir,2)) as saldo_akhir, CONCAT(tgl_mulai, 'T17:00:00.000Z') AS tgl_mulai, jkw, CONCAT(tgl_jt, 'T17:00:00.000Z') AS tgl_jt, aro, `type` FROM vkmi_simpanan_tabungan"
                )
            );

            $queryDeposito = DB::select(
                DB::raw(
                    "SELECT nasabah_id, nama_nasabah, no_rekening, saldo_akhir, CONCAT(tgl_mulai, 'T17:00:00.000Z') AS tgl_mulai, jkw, CONCAT(tgl_jt, 'T17:00:00.000Z') AS tgl_jt, aro, `type` FROM vkmi_simpanan_deposito"
                )
            );

            $tabungan = array();
            $deposito = array();
            $i = 0;
            $j = 0;

            foreach ($queryTabungan as $key => $val) {
                $tabungan[$i] = [
                    "nasabah_id"   => $val->nasabah_id,
                    "nama_nasabah" => $val->nama_nasabah,
                    "no_rekening"  => $val->no_rekening,
                    "saldo_akhir"  => (int) $val->saldo_akhir,
                    "tgl_mulai"    => $val->tgl_mulai,
                    "jkw"          => $val->jkw,
                    "tgl_jt"       => $val->tgl_jt,
                    "aro"          => $val->aro,
                    "type"         => $val->type
                ];
                $i++;
            }

            foreach ($queryDeposito as $key => $val) {
                $deposito[$j] = [
                    "nasabah_id"   => $val->nasabah_id,
                    "nama_nasabah" => $val->nama_nasabah,
                    "no_rekening"  => $val->no_rekening,
                    "saldo_akhir"  => (int) $val->saldo_akhir,
                    "tgl_mulai"    => $val->tgl_mulai,
                    "jkw"          => (int) $val->jkw,
                    "tgl_jt"       => $val->tgl_jt,
                    "aro"          => (int) $val->aro,
                    "type"         => $val->type
                ];
                $j++;
            }

            return response(
            [
                [
                    "type" => "tabungan",
                    "data" => $tabungan
                ]
                ,
                [
                    "type" => "deposito",
                    "data" => $deposito
                ]
            ], 201);

        }catch(\Exception $e){
            return response()->json([
                'status'=>'error',
                'message'=> $e
            ], 412);
        }
    }

    public function saDeposit($no_rekening, $tanggal){
        if(preg_match("/[[:digit:]]/", $no_rekening)){
            if(preg_match("/[[:digit:]]/", $tanggal)){
                try {
                    $query = DB::select("SELECT SUM(IF(FLOOR(my_kode_trans/100)=2, IFNULL(pokok_trans,0.00), 0)) AS debet, SUM(IF(FLOOR(my_kode_trans/100)=1, IFNULL(pokok_trans,0.00), 0)) AS kredit FROM deptrans WHERE no_rekening=? AND tgl_trans <= ? ", [$no_rekening, $tanggal]);

                    $data = [
                        'debet'  => (int) $query[0]->debet,
                        'kredit' => (int) $query[0]->kredit
                    ];

                    return response([
                        "type" => "saldo deposit",
                        'data' => [$data]
                    ], 201);
                }catch(\Exception $e){
                    return response()->json([
                           'status'=>'error',
                           'message'=> $e
                    ], 412);
                }
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=> 'The date must be a number, ex: 2007-08-31'
                ], 400);
            }
        }else{
            return response()->json([
                'status'=>'error',
                'message'=> 'Account(Bill) must be a number, ex: 8010225508'
            ], 400);
        }
    }

    public function detailTransaksi($no_rekening, $tanggalStart, $tanggalEnd){
        if(preg_match("/[[:digit:]]/", $no_rekening)){
            if(preg_match("/[[:digit:]]/", $tanggalStart)){
                if(preg_match("/[[:digit:]]/", $tanggalEnd)){
                    try {
                        $queryOne = DB::select("SELECT SUM(IF(FLOOR(my_kode_trans/100)=2, IFNULL(pokok_trans,0.00)+IFNULL(bunga_trans,0.00), 0)) AS debet, SUM(IF(FLOOR(my_kode_trans/100)=1, IFNULL(pokok_trans,0.00)+IFNULL(bunga_trans,0.00), 0)) AS kredit FROM deptrans WHERE no_rekening=? AND tgl_trans BETWEEN ? AND ?", [$no_rekening, $tanggalStart, $tanggalEnd]);

                        $query = DB::select("SELECT no_rekening, tgl_trans, IFNULL(jam,'00:00:00') AS jam, keterangan, IF(FLOOR(my_kode_trans/100)=1, IFNULL(pokok_trans,0.00)+IFNULL(bunga_trans,0.00), -(IFNULL(pokok_trans,0.00)+IFNULL(bunga_trans,0.00))) AS jumlah FROM deptrans WHERE no_rekening=? AND tgl_trans BETWEEN ? AND ? ORDER BY tgl_trans DESC, IFNULL(jam,'00:00:00') DESC", [$no_rekening, $tanggalStart, $tanggalEnd]);


                        $arrData = array();
                        $data = array();
                        $i = 0;
                        $j = 0;

                        foreach($query as $key => $val){
                            $arrData[$i]['no_rekening'] = $val->no_rekening;
                            $arrData[$i]['tgl_trans']   = $val->tgl_trans.'T17:00:00.000Z';
                            $arrData[$i]['jam']         = $val->jam;
                            $arrData[$i]['keterangan']  = $val->keterangan;
                            $arrData[$i]['jumlah']      = (int)$val->jumlah;
                            $i++;
                        }

                        // Group data by the "tgl_trans" key
                        $byGroup = $this->group_by("tgl_trans", $arrData);

                        foreach ($byGroup as $key => $val) {
                            $data[$j]['tanggal_transaksi'] = $val['tanggal_transaksi'];
                            $data[$j]['data'] = $val['data'];
                            $j++;
                        }

                        return response([
                            'debet' => $queryOne[0]->debet == null || $queryOne[0]->debet == '' ? null : (int) $queryOne[0]->debet,
                            'kredit' => $queryOne[0]->kredit == null || $queryOne[0]->kredit == '' ? null : (int) $queryOne[0]->kredit,
                            'log_transaksi' =>
                                $data
                        ], 200);
                    }catch(\Exception $e){
                        return response()->json([
                               'status'=>'error',
                               'message'=> $e
                        ], 412);
                    }
                }else{
                    return response()->json([
                        'status'=>'error',
                        'message'=> 'The date-end must be a number, ex: 2007-08-31'
                    ], 400);
                }
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=> 'The date-start must be a number, ex: 2007-08-31'
                ], 400);
            }
        }else{
            return response()->json([
                'status'=>'error',
                'message'=> 'Account(Bill) must be a number, ex: 8010225508'
            ], 400);
        }
    }

    public function saldoTabungan($no_rekening, $tanggalTrans){
        if(preg_match("/[[:digit:]]/", $no_rekening)){
            if(preg_match("/[[:digit:]]/", $tanggalTrans)){
                try {
                    $query = DB::select("SELECT CONVERT(FLOOR(ROUND(SUM(IF(FLOOR(my_kode_trans/100)=2, pokok, 0.00)),2)), SIGNED) AS debet, CONVERT(FLOOR(ROUND(SUM(IF(FLOOR(my_kode_trans/100)=1, pokok, 0.00)),2)), SIGNED) AS kredit FROM tabtrans WHERE no_rekening=? AND tgl_trans >= DATE_ADD(?, INTERVAL -1 MONTH) AND tgl_trans <= ?", [$no_rekening, $tanggalTrans, $tanggalTrans]);

                }catch(\Exception $e){
                    return response()->json([
                        'status'=>'error',
                        'message'=> $e
                    ], 412);
                }

                return response([
                    'type' => 'saldo debet dan kredit tabungan',
                    'data' => $query
                ], 200);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=> 'The date-start must be a number, ex: 08042019'
                ], 400);
            }
        }else{
            return response()->json([
                'status'=>'error',
                'message'=> 'Account(Bill) must be a number, ex: 8010225508'
            ], 400);
        }
    }

    public function dtt($no_rekening, $tanggalStart, $tanggalEnd){
        if(preg_match("/[[:digit:]]/", $no_rekening)){
            if(preg_match("/[[:digit:]]/", $tanggalStart)){
                if(preg_match("/[[:digit:]]/", $tanggalEnd)){
                    try {

                        $queryOne = DB::select("SELECT FLOOR(ROUND(SUM(IF(FLOOR(my_kode_trans/100)=2, pokok, 0)),2)) AS debet, FLOOR(ROUND(SUM(IF(FLOOR(my_kode_trans/100)=1, pokok, 0)),2)) AS kredit FROM tabtrans WHERE no_rekening=? AND tgl_trans BETWEEN ? AND ? ORDER BY tgl_trans DESC, tabtrans_id DESC;",[$no_rekening, $tanggalStart, $tanggalEnd]);

                        $query = DB::select("SELECT no_rekening, CONCAT(tgl_trans, 'T17:00:00.000Z') AS tgl_trans, IFNULL(jam,'00:00:00') AS jam, keterangan, FLOOR(ROUND(IF(FLOOR(my_kode_trans/100)=1, pokok, -pokok),2)) AS jumlah FROM tabtrans WHERE no_rekening=? AND tgl_trans BETWEEN ? AND ? ORDER BY tgl_trans DESC, tabtrans_id DESC", [$no_rekening, $tanggalStart, $tanggalEnd]);

                        $arrData = array();
                        $data = array();
                        $i = 0;
                        $j = 0;

                        foreach($query as $key => $val){
                            $arrData[$i]['no_rekening'] = $val->no_rekening;
                            $arrData[$i]['tgl_trans']   = $val->tgl_trans;
                            $arrData[$i]['jam']         = $val->jam;
                            $arrData[$i]['keterangan']  = $val->keterangan;
                            $arrData[$i]['jumlah']      = (int)$val->jumlah;
                            $i++;
                        }

                        // Group data by the "tgl_trans" key
                        $byGroup = $this->group_by("tgl_trans", $arrData);

                        foreach ($byGroup as $key => $val) {
                            $data[$j]['tanggal_transaksi'] = $val['tanggal_transaksi'];
                            $data[$j]['data'] = $val['data'];
                            $j++;
                        }

                        return response([
                            'debet' => $queryOne[0]->debet==null || $queryOne[0]->debet=='' ? $queryOne[0]->debet : (int) $queryOne[0]->debet,
                            'kredit' => $queryOne[0]->kredit==null || $queryOne[0]->kredit=='' ? $queryOne[0]->kredit : (int) $queryOne[0]->kredit,
                            'log_transaksi' =>
                                $data
                        ], 200);

                    } catch (\Exception $e) {
                        return response()->json([
                            'status'=>'error',
                            'message'=> $e
                        ], 412);
                    }
                }else{
                    return response()->json([
                        'status'=>'error',
                        'message'=> 'The date-end must be a number, ex: 2007-08-31'
                    ], 400);
                }
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=> 'The date-start must be a number, ex: 2007-08-31'
                ], 400);
            }
        }else{
            return response()->json([
                'status'=>'error',
                'message'=> 'Account(Bill) must be a number, ex: 8010225508'
            ], 400);
        }
    }

    public function kredit(Request $req){
        $user_id = $this->GTI($req);
        $user = User::where('id', $user_id)->first();
        $nasabah_id = $user->nasabah_id;

        try {
            DB::statement(DB::raw("SET @pv_nasabah_id = '$nasabah_id' "));
            $query = DB::select(
                DB::raw(
                    "SELECT `nasabah_id`, `no_rekening`, `nama_nasabah`,
                        FLOOR(ROUND(`saldo_akhir`, 2)) as saldo_akhir,
                        concat(tgl_realisasi,'T00:00:00.000Z') as tgl_realisasi,
                        `jkw`,
                        `satuan_waktu`,
                        concat(tgl_jatuh_tempo,'T00:00:00.000Z') as tgl_jatuh_tempo,
                        IF(`kolektibilitas`=1,'1 - Lancar',
                            IF(`kolektibilitas`=2,'2 - Kurang Lancar',
                                IF(`kolektibilitas`=3,'3 - Diragukan','4 - Macet')
                            )
                        ) as `kolektibilitas`,
                        `type`,

                        IF(`tunggakan` < 0, 0, CONVERT(ROUND(`tunggakan`,2), SIGNED)) as `tunggakan`,
                        IF(`tagihan` < 0, 0, CONVERT(ROUND(`tagihan`,2), SIGNED)) as `tagihan`,
                        IF(`denda` < 0, 0, CONVERT(ROUND(`denda`,2), SIGNED)) as `denda`,
                        IF(`tunggakan` < 0, 0, CONVERT(ROUND(`tunggakan`,2), SIGNED))  +
                        IF(`tagihan` < 0, 0, CONVERT(ROUND(`tagihan`,2), SIGNED)) +
                        IF(`denda` < 0, 0, CONVERT(ROUND(`denda`,2), SIGNED)) AS `total`
                    FROM vkmi_kredit"
                )
            );

            $arrNewSku = array();
            $incI = 0;
            foreach($query as $key){
                $arrNewSku[$incI]['nasabah_id']      = $key->nasabah_id;
                $arrNewSku[$incI]['no_rekening']     = $key->no_rekening;

                $arrNewSku[$incI]['nama_nasabah']    = $key->nama_nasabah;
                $arrNewSku[$incI]['saldo_akhir']     = (int) $key->saldo_akhir;
                $arrNewSku[$incI]['tgl_realisasi']   = $key->tgl_realisasi;
                $arrNewSku[$incI]['jkw']             = (int) $key->jkw;
                $arrNewSku[$incI]['satuan_waktu']    = $key->satuan_waktu;
                $arrNewSku[$incI]['tgl_jatuh_tempo'] = $key->tgl_jatuh_tempo;
                $arrNewSku[$incI]['kolektibilitas']  = $key->kolektibilitas;
                $arrNewSku[$incI]['type']            = $key->type;
                $arrNewSku[$incI]['tunggakan']       = (int) $key->tunggakan;
                $arrNewSku[$incI]['tagihan']         = (int) $key->tagihan;
                $arrNewSku[$incI]['denda']           = (int) $key->denda;
                $arrNewSku[$incI]['total']           = (int) $key->total;
                // $arrNewSku[$incI]['nomor']['nasabah_idklo'] = $key;
                // $arrNewSku[$incI]['nasabah_id'] = $key->;
                $incI++;
            }

            return response(
                $arrNewSku
            , 200);
        } catch (\Exception $e) {
            return response()->json([
                'status'=>'error',
                'message'=> $e
            ], 412);
        }
    }

    public function riwayatKredit($no_rekening){
        if(preg_match("/[[:digit:]]/", $no_rekening)){
            try {
                $query = DB::select("SELECT angke, concat(tgl_jt_tempo,'T00:00:00.000Z') as tgl_jt_tempo, FLOOR(ROUND(pokok+bunga,2)) AS tagihan, concat(tgl_bayar,'T00:00:00.000Z') as tgl_bayar, FLOOR(ROUND(byr_pokok+byr_bunga,2)) AS pembayaran, ".
                "pokok+bunga=byr_pokok+byr_bunga AS islunas FROM test WHERE no_rekening=?", [$no_rekening]);
            } catch (\Exception $e) {
                return response()->json([
                    'status'=>'error',
                    'message'=> $e
                ], 412);
            }

            return response(
                $query
            , 200);
        }else{
            return response()->json([
                'status'=>'error',
                'message'=> 'Account(Bill) must be a number, ex: 00-02-00257-10 atau 00020025710'
            ], 400);
        }
    }

    public function up_avatar(uploadImageRequest $req){

        $user_id = $this->GTI($req);
        $oldData = User::where('id', $user_id)->first();
        $oldImg = $oldData->avatar_user;

        if ($file = $req->file('uploadFile')) {

            if (empty($file)) {
                return response()->json(["message" => "No files were uploaded."], 400);
            }

            $path = 'public/images/uploaded_images';
            $name = $user_id.'avatarprofile.'.$file->getclientoriginalextension();

            if($file->getClientSize() > intval(1024 * 1024 * 2)){
                return response()->json(["message" => "Foto maksimal 2MB !"], 400);
            }

            if(!$file->getClientMimeType()){
                return response()->json(["message" => "Harap upload file dengan extensi jpeg/jpg/png !!!"], 400);
            }

            try {
                if(!empty($oldImg))
                {
                    File::delete($oldImg);
                }

                $file->move($path, $name);

                $avatar_URL = 'public/images/uploaded_images/'.$name;

                User::where('id', $user_id)->update(['avatar_user' => $avatar_URL]);

                return response()->json(['message' => 'upload success'], 200);

            } catch (Exception $e) {

                return response()->json(["message" => "Cek kembali gambar yang anda pilih"], 400);
            }
        }else{
            $req->uploadFile = $oldImg;
        }
    }

    public function avatar(Request $req){
        $user_id = $this->GTI($req);

        $data = User::where('id', $user_id)->first();

        $content = File::get($data->avatar_user);

        // $content = readfile($data->avatar_user);

        if($data->avatar_user != null){
            return response($content, 200)->header('Content-Type','image/jpg/png');
        }else{
            return response('No such image', 400)->header('Content-Type', 'text/html');
        }

        // ob_end_clean();
    }

    public function editProfile(editProfileRequest $req){

        $user_id = $this->GTI($req);

        $show = DB::table('kmi_user')
                    ->where('id', $user_id)
                    ->first();

        $dataArray = array(
            'no_hp'    => empty($req->no_hp)    ? $show->no_hp    : $req->no_hp,
            'fullname' => empty($req->fullname) ? $show->fullname : $req->fullname,
            'no_ktp'   => empty($req->no_ktp)   ? $show->no_ktp   : $req->no_ktp,
            'alamat'   => empty($req->alamat)   ? $show->alamat   : $req->alamat
        );

        $queryUpdate = DB::table('kmi_user')->where('id', $user_id)->update($dataArray);

        if ($queryUpdate) {
            return response()->json([
                'id' => (int) $show->id,
                'fullname' => $show->fullname,
                'nasabah_id' => $show->nasabah_id,
                'link' => $show->link,
                'alamat' => $show->alamat,
                'no_ktp' => $show->no_ktp,
                'no_hp' => $show->no_hp,
                'no_hp_verified' => (int) $show->no_hp_verified,
                'email' => $show->email,
                'email_verified' => $show->email_verified,
                'avatar_user' => $show->avatar_user
            ], 201);
        }else{
            return response()->json([
                'message'=> 'Tidak ada perubahan'
            ], 201);
        }
    }

    public function smsToken(OTP $OTP,Request $req){
        $user_id = $this->GTI($req);
        $queryHP = DB::table('kmi_user')->select('no_hp')->where('id', $user_id)->get();
        $dataHp = $queryHP[0]->no_hp;

        // $url = 'API_SMS/sms.php';

        $kode_otp = rand(1000, 9999);

        $inData = $OTP->curl_post($dataHp, $kode_otp);

        $outData = json_decode($inData, true);

        $xData = $outData['messages'][0]['smsCount'];

        if ($xData == 1) {
            $queryUpdate = DB::update("UPDATE kmi_user SET kode_otp=? WHERE `id`=?",[$kode_otp, $user_id]);

            return response()->json($outData, 200);
        }else{
            return response()->json([
                'message'=> 'failed to send OTP, check your connection'
            ], 400);
        }
    }

    public function changePhone($no_hp, OTP $OTP,Request $req){
        $user_id = $this->GTI($req);

        $check = DB::table('kmi_user')->select('no_hp')->where('id', '!=', $user_id)->where('no_hp', $no_hp)->first();

        $dataHp = $no_hp;

        $kode_otp = rand(1000, 9999);

        $msg_otp = '[KMI-MITRA] Your OTP code: '.$kode_otp;

        if(preg_match("/[[:digit:]]/", $no_hp)){
            if (strlen($no_hp) == 10 || strlen($no_hp) == 11 || strlen($no_hp) == 12 || strlen($no_hp) == 13){
                if ($check != null) {
                    return response()->json([
                        'message'=> 'No Hp sudah digunakan oleh orang lain'
                    ], 400);
                }else{
                    $inData = $OTP->curl_post($dataHp, $msg_otp);

                    $outData = json_decode($inData, true);

                    $xData = $outData['messages'][0]['smsCount'];

                    if ($xData == 1) {

                        $queryUpdate = DB::update("UPDATE kmi_user SET kode_otp=?, no_hp=? WHERE `id`=?",[$kode_otp, $dataHp, $user_id]);

                        return response()->json(['message' => 'otp sent'], 201);
                    }else{
                        return response()->json([
                            'message'=> 'failed to send OTP, check your connection'
                        ], 412);
                    }
                }
            }else{
                return response()->json([
                    'message'=> 'Mobile number must be between 10-13 digits, ex: 081212408249'
                ], 400);
            }
        }else{
            return response()->json([
                'message'=> 'Mobile number must be a number, ex: 081212408249'
            ], 400);
        }
    }

    public function otp_kode($otp_kode, Request $req){
        $user_id = $this->GTI($req);

        if(preg_match("/[[:digit:]]/", $otp_kode)) {
            if (strlen($otp_kode) == 4) {
                $query = DB::table('kmi_user')->select('kode_otp')->where('kode_otp', $otp_kode)->where('id', $user_id)->first();

                if ($query != null) {

                    DB::update("UPDATE kmi_user SET no_hp_verified ='1' WHERE `id`=? AND `kode_otp`=?",[$user_id, $otp_kode]);

                    return response()->json([
                        'status'=>'success',
                        'message'=> 'OTP verification successful'
                    ], 200);
                }else{
                    return response()->json([
                        'status'=>'error',
                        'message'=> 'The OTP number does not match your current login ID'
                    ], 400);
                }

            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=> 'OTP number must be 4 digits, ex: 1987'
                ], 400);
            }

        }else {
            return response()->json([
                'status'=>'error',
                'message'=> 'OTP number must be a number, ex: 1987'
            ], 400);
        }
    }

    public function ajukankredit(Request $req){
        $user_id = $this->GTI($req);
        $query = DB::table('kmi_user')->where('id', $user_id)->first();

        if ($query) {
            $view    = 'email.emailpengajuankredit';
            $from    = false; // 'noreply@kreditmandiri.co.id'
            $to      = 'aplikasi@kreditmandiri.co.id';
            $cc      = false;
            $subject = 'Pengajuan Kredit';

            $files = array();
            for ($x = 1; $x <= 5; $x++) {
                $files[$x] = $req->file("attach$x");
            }

            $msg = array(
                'id_user'    => $query->id,
                'fullname'   => $query->fullname,
                'noktpuser'  => $query->no_ktp,
                'alamatuser' => $query->alamat,
                'nohpuser'   => $query->no_hp,
                'emailuser'  => $query->email
            );

            $send = $this->mail($view, $from, $to, $cc, $subject, $msg, $files);

            try {
                return response([
                    'status' => 'success',
                    'message' => 'Credit application successfully sent'
                ], 201);
            } catch (Exception $e) {
                return response([
                    'status' => 'error',
                    'message' => 'Credit application failed to send'
                ], 400);
            }
        }else{
            return response([
                'status' => 'error',
                'message' => 'Your email not ready'
            ], 412);
        }
    }
}
