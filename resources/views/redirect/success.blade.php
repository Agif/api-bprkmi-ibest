<!DOCTYPE html>
<html>

<head>
  <title>BPR Mandiri Indonesia</title>
</head>


  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Bitter:400,700);
    @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
    @import url('https://fonts.googleapis.com/css?family=Cabin');
    @import url('https://fonts.googleapis.com/css?family=Poppins');
    @import url('https://fonts.googleapis.com/css?family=Roboto+Slab');
    #outlook a {
      padding: 0
    }

    blockquote .original-only,
    .WordSection1 .original-only {
      display: none !important
    }

    @media only screen and (max-width: 480px) {
      body,
      table,
      td,
      p,
      a,
      li,
      blockquote {
        -webkit-text-size-adjust: none !important
      }
      body {
        width: 100% !important;
        min-width: 100% !important
      }
      #template-container {
        margin-top: 0px !important;
        max-width: 600px !important;
        width: 100% !important
      }
      #header {
        padding: 30px 20px 30px 20px !important
      }
      #content {
        padding: 0 20px 20px !important
      }
      #content img.full-width {
        width: 100% !important
      }
      #signature-row {
        width: 100% !important
      }
      #signature-row .signature-box {
        display: block !important;
        width: 100% !important;
        margin-top: 20px !important
      }
    }

    @media only screen and (max-width: 480px) {
      #template-container {
        margin-top: 0px !important;
        max-width: 600px !important;
        width: 100% !important
      }
      #header {
        padding: 30px 20px 30px 20px !important
      }
      #content {
        padding: 20px 20px 20px !important
      }
      #content .full-width {
        width: 100% !important
      }
      table.collapse {
        width: 100% !important
      }
      table.collapse td {
        width: 100% !important;
        display: block !important
      }
      #zenpayroll-banner {
        padding-left: 20px;
        padding-right: 20px
      }
      #zenpayroll-banner td:nth-child(1) {
        text-align: center
      }
      #zenpayroll-banner td:nth-child(2) p {
        margin-left: 20px
      }
      #signature-row {
        width: 100% !important
      }
      #signature-row .signature-box {
        display: block !important;
        width: 100% !important;
        margin-top: 20px !important
      }
    }
  </style>
  <body style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0;">
  <table id="body-table" style="border-collapse: collapse; margin-left: 0; margin-top: 0; margin-right: 0; margin-bottom: 0; padding-left: 0; padding-bottom: 0; padding-right: 0; padding-top: 0; background-color: #f2f2f2; height: 100% !important; width: 100% !important;"
    bgcolor="#f2f2f2">
    <tbody>
      <tr>
        <td align="center" style="border-collapse: collapse;">
          <!-- <p id="template-logo" style="text-align: center; margin-right: 0; margin-top: 30px; margin-bottom: 30px; margin-left: 0;" align="center">
            <a href="https://gusto.com" style="color:#59595b">
              <img src="http://103.234.254.186/download/images/BPR-LOGO.jpg" id="logo" alt="Gusto" style="height: auto; line-height: 100%; outline: none; text-decoration: none; border-bottom-style: none; border-right-style: none; border-top-style: none; border-left-style: none; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0;"
                width="120">
            </a>
          </p> -->
          <!-- BEGIN
            TEMPLATE // -->
          <table id="template-container" style="border-collapse: collapse; margin-top: 50px;  width: 600; background-color: #FFFFFF; border-top-color: #3D9970; border-top-width: 10px; border-top-style: solid;" bgcolor="#FFFFFF" width="600" >
            <tbody>
              <tr>
                <td id="content" style="border-collapse: collapse; padding-right: 40px; padding-top: 20px; padding-bottom: 20px; padding-left: 40px;">
                  <div>
                    <!-- <a style="color:#59595b">
                      <img src="http://103.234.254.186/download/images/BPR-LOGO.jpg" alt="logo bpr" id="bpr-logo" class="full-width" style="height: auto; line-height: 100%; outline: none; text-decoration: none; border-bottom-style: none; border-right-style: none; border-top-style: none; border-left-style: none; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0;"
                        width="520">
                    </a> -->
                  </div>
                  <a href="#">
                  <img src="http://103.234.254.186/download/images/divider.png" alt="divider" id="bpr-logo" class="full-width" style="height: auto; line-height: 100%; height: 20px; outline: none; text-decoration: none; border-bottom-style: none; border-right-style: none; border-top-style: none; border-left-style: none; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0;"
                        width="520">
                  </a>
                  <p class="heading" style="font-family: 'Roboto Slab', serif; font-weight: 700; font-size: 24px; color: #59595b; margin-bottom: 20px; text-align: center;" align="center">KONFIRMASI EMAIL BERHASIL</p>
                  <p class="heading" style="font-family: 'Cabin', sans-serif; font-weight: 500; font-size: 14px; color: #59595b; margin-bottom: 20px; text-align: center;" align="center">Selamat, kini anda telah menjadi bagian dari KMI IBEST<br><br> <a style="color: #333; text-decoration: none" href="https://play.google.com/store/apps/details?id=com.kreditmandiri.nasabah&hl=en">Download app from playstore</a> <br>
                  	<a href="https://play.google.com/store/apps/details?id=com.google.android.apps.maps" target="_blank" style="color:#59595b">
		              <img src="http://103.234.254.186/download/images/google-play-badge.png" id="logo" alt="Gusto" style="height: auto; line-height: 100%; outline: none; text-decoration: none; border-bottom-style: none; border-right-style: none; border-top-style: none; border-left-style: none; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0;"
		                width="120">
		            </a>
                  </p>

                  <div class="cta margin-top-30px margin-bottom-50px" style="text-align: center; margin-top: 10px !important; margin-bottom: 50px !important;" align="center">

                  </div>
                  <table id="have-questions" class="full-width margin-top-20px" style="border-collapse: collapse; margin-top: 10px !important;" bgcolor="#1c4b77" width="520">
                    <tr>
                      <td style="border-collapse: collapse; background-color:#3D9970; text-align: center; border-top-color: #3D9970; border-top-width: 10px; border-top-style: solid;" bgcolor="#1c4b77">
                        <p class="title margin-bottom-10px" style="color: #f5f5f5; font-family: 'Cabin', sans-serif; font-weight: 700; font-size: 16px; text-align: center; margin-bottom: 5px !important;" align="center"><strong>BUTUH BANTUAN?</strong>
                        </p>
                        <p class="call-us margin-top-none" style="margin-bottom: 20px; color: #FFFFFF; font-family: 'Cabin', sans-serif; font-size: 12px; text-align: center; margin-top: 0 !important;" align="center">Silahkan hubungi kami<br> &#9743;
                          <a href="tel:02127070000" style="color:#FFFFFF;  text-decoration: none !important;">(021) 2707-0000</a><br> &#9993;
                          <a href="mailto:info@kreditmandiri.co.id" style="color:#FFFFFF;  text-decoration: none !important;">info@kreditmandiri.co.id</a>
                        </p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <!-- // END TEMPLATE -->
          <div id="template-footer" style="padding-top:10px; padding-right: 20px; padding-bottom: 50px; padding-left: 20px;">
            <div id="footer-copyright" style="font-family: 'Cabin', sans-serif; color: #59595b; font-size: 14px;">&#169; 2018 BPR Kredit Mandiri Indonesia</div>
            <div id="footer-address" style="font-family: 'Cabin', sans-serif; color: #59595b; font-size: 14px;">Jl. Raya Karang Raya Satria NO.3 Karang Satria<br>Tambun Utara - Bekasi</div>
<!--             <div id="footer-links" class="margin-top-10px" style="font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #59595b; font-size: 14px; margin-top: 10px !important;">
              <a href="http://kreditmandiri.co.id/?page_id=2" style="color:#59595b">Privacy Policy</a>
            </div> -->
            <!-- social button link <div id="footer-social" class="padding-top-20px" style="font-family: 'Open Sans', Helvetica, Arial, sans-serif; color: #59595b; font-size: 14px; padding-top: 20px !important;">
              <a href="https://www.facebook.com/gustohq" style="color:#59595b;text-decoration:none;">
                <img src="https://assets.zenpayroll.com.s3.amazonaws.com/static/emails/assets/6c18017504374518b50a308236d0f7eb/social-facebook.png" class="social-icon" alt="Facebook" style="height: auto; line-height: 100%; outline: none; text-decoration: none; border-bottom-style: none; border-right-style: none; border-top-style: none; border-left-style: none; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0;"
                  width="30" height="30" hspace="7">
              </a>
              <a href="https://twitter.com/gustohq" style="color:#59595b; text-decoration:none;">
                <img src="https://assets.zenpayroll.com.s3.amazonaws.com/static/emails/assets/6c18017504374518b50a308236d0f7eb/social-twitter.png" class="social-icon" alt="Twitter" style="height: auto; line-height: 100%; outline: none; text-decoration: none; border-bottom-style: none; border-right-style: none; border-top-style: none; border-left-style: none; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0;"
                  width="30" height="30" hspace="7">
              </a>
            </div> -->
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</body>

</html>