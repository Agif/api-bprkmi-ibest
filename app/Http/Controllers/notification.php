<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

class Notification extends Controller
{
    public function curl_post($to, $title, $message){
        define('API_ACCESS_KEY','AAAAjrvLI_4:APA91bGI_urQhVNWgEMEReiqUG8Jz3o8pXX55T69mDGv9KW-BwphHdsk4E74UUkx4kb3XqUfA_QMu_QjWAJw3PLg2eovQtqD2hCfJhHFMdxfptKlvP0ZTW6hC9XgB06KBmuvi45LU9nA'); //Server Key on SERVER

        $url = 'https://fcm.googleapis.com/fcm/send';

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        // $fcm_token = array( $_GET['id'] );

        $notification = [
            'title' => $title,
            'body'  => $message,
            'icon'  => 'stock_ticker_update',
            'color' => '#f45342',
            'sound' => 'default'
        ];

        $data = [
            'to'            => $to, //as token
            'notification'  => $notification
        ];

        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];

        $options = array(
            'http' => array(
                'header'  => $headers,
                'method'  => 'POST',
                'content' => json_encode($data)
            )
        );

        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);

        return $result;
    }
}
