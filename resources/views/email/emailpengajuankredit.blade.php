
<!DOCTYPE html>
<html>

<head>
  <meta content="text/html; charset=US-ASCII" http-equiv="Content-Type">
  <title>BPR Mandiri Indonesia</title>
</head>


  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Bitter:400,700);
    @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
    @import url('https://fonts.googleapis.com/css?family=Cabin');
    @import url('https://fonts.googleapis.com/css?family=Poppins');
    #outlook a {
      padding: 0
    }

    blockquote .original-only,
    .WordSection1 .original-only {
      display: none !important
    }

    @media only screen and (max-width: 480px) {
      body,
      table,
      td,
      p,
      a,
      li,
      blockquote {
        -webkit-text-size-adjust: none !important
      }
      body {
        width: 100% !important;
        min-width: 100% !important
      }
      #template-container {
        margin-top: 0px !important;
        max-width: 600px !important;
        width: 100% !important
      }
      #header {
        padding: 30px 20px 30px 20px !important
      }
      #content {
        padding: 0 20px 20px !important
      }
      #content img.full-width {
        width: 100% !important
      }
      #signature-row {
        width: 100% !important
      }
      #signature-row .signature-box {
        display: block !important;
        width: 100% !important;
        margin-top: 20px !important
      }
    }

    @media only screen and (max-width: 480px) {
      #template-container {
        margin-top: 0px !important;
        max-width: 600px !important;
        width: 100% !important
      }
      #header {
        padding: 30px 20px 30px 20px !important
      }
      #content {
        padding: 20px 20px 20px !important
      }
      #content .full-width {
        width: 100% !important
      }
      table.collapse {
        width: 100% !important
      }
      table.collapse td {
        width: 100% !important;
        display: block !important
      }
      #zenpayroll-banner {
        padding-left: 20px;
        padding-right: 20px
      }
      #zenpayroll-banner td:nth-child(1) {
        text-align: center
      }
      #zenpayroll-banner td:nth-child(2) p {
        margin-left: 20px
      }
      #signature-row {
        width: 100% !important
      }
      #signature-row .signature-box {
        display: block !important;
        width: 100% !important;
        margin-top: 20px !important
      }
    }
  </style>
  <body style="margin-top: 0; margin-right: 0; margin-bottom: 0; margin-left: 0; padding-top: 0; padding-right: 0; padding-bottom: 0; padding-left: 0;">
  <table id="body-table" style="border-collapse: collapse; margin-left: 0; margin-top: 0; margin-right: 0; margin-bottom: 0; padding-left: 0; padding-bottom: 0; padding-right: 0; padding-top: 0; background-color: #f2f2f2; height: 100% !important; width: 100% !important;"
    bgcolor="#f2f2f2">
    <tbody>
      <tr>
        <td align="center" style="border-collapse: collapse;">
          <p id="template-logo" style="text-align: center; margin-right: 0; margin-top: 30px; margin-bottom: 30px; margin-left: 0;" align="center">
          <table id="template-container" style="border-collapse: collapse; margin-top: 0px; width: 600px; background-color: #FFFFFF; border-top-color: #f00740; border-top-width: 10px; border-top-style: solid;" bgcolor="#FFFFFF" width="600">
            <tbody>
              <tr>
                <td id="content" style="border-collapse: collapse; padding-right: 40px; padding-top: 20px; padding-bottom: 20px; padding-left: 40px;">
                  <div>
                  </div>
                  <p class="heading" style="font-family: 'Cabin', sans-serif; font-weight: 700; font-size: 16px; color: #59595b; margin-bottom: 20px; text-align: center;" align="center">INFORMASI PENGAJUAN KREDIT</p>
                  <p class="heading" style="font-family: 'Cabin', sans-serif; font-weight: 500; font-size: 14px; color: #59595b; margin-bottom: 20px; text-align: left;" align="center">
                    Nama lengkap&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<strong>{{ $fullname }}</strong><br>
                    Id user&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<strong>&nbsp;{{ $id_user }}</strong><br>
                  Alamat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<strong>&nbsp;{{ $alamatuser }}</strong><br>
                  No. ktp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<strong>&nbsp;{{ $noktpuser }}</strong><br>
                  No. telpon&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<strong>&nbsp;{{ $nohpuser }}</strong><br>
                  Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<strong>&nbsp;{{ $emailuser }}</strong><br></p>
                  <div class="cta margin-top-30px margin-bottom-50px" style="text-align: center; margin-top: 30px !important; margin-bottom: 50px !important;" align="center">

                            <div id="template-footer" style="padding-top:0; padding-right: 20px; padding-bottom: 50px; padding-left: 20px;">
            <div id="footer-copyright" style="font-family: 'Cabin', sans-serif; color: #59595b; font-size: 10px;">&#169; 2018 BPR Kredit Mandiri Indonesia Jl. Raya Karang Raya Satria NO.3 Karang Satria Tambun Utara - Bekasi</div>
              </tr>
            </tbody>
          </table>

          </div>
        </td>
      </tr>
    </tbody>
  </table>
</body>

</html>
