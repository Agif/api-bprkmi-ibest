<?php

namespace App\Http\Middleware;

use DB;
use App\User;
use Closure;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class JwtMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        // $token = $request->header('Authorization'); // get token from request header
        // $token = $request->get('token');
        
        $token = $request->header('Authorization');

        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'status' => 401,
                'error' => 'Token required.'
            ], 401);
        }else

        // $jwtSTR = stripos($token, 'JWT');

        // if ($jwtSTR !== FALSE){
        if(preg_match("/JWT /", $token)){

            $strToken = str_replace('JWT ','', $token);
        
            try {
                $credentials = JWT::decode($strToken, env('JWT_SECRET'), ['HS256']);
            
            } catch(ExpiredException $e) {
            
                return response()->json([
                    'error' => 'Provided token is expired.'
                ], 400);
            
            } catch(Exception $e) {
                return response()->json([
                    'error' => 'An error while decoding token.'
                ], 400);
            }
        
            $user = User::find($credentials->id);

            $imei = DB::table('kmi_user')->select('imei')->where('id', $credentials->id)->first();

            if ($imei->imei == null || $imei->imei == '') {
                return response()->json([
                    'message'=> 'Anda harus login terlebih dahulu'
                ], 412);
            }
        
            // Now let's put the user in the request class so that you can grab it from there
            if(!empty($user)){
            
                $request->auth = $user;
            
            }else{
            
                return response()->json([
                    'error' => 'Provided token is invalid.'
                ], 400);
            }
            
            return $next($request);
        }else{
            return response()->json([
                'status' => 400,
                'error' => 'Token wrong !!!.'
            ], 400);
        }
    }

    protected $except = [
        'user/verify/*'
        // 'stripe/*',
        // 'http://example.com/foo/bar',
        // 'http://example.com/foo/*',
    ];
}