<?php

namespace App\Http\Controllers\Publics;

use App\Http\Controllers\Notification;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Firebase;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Traits\TokenId;
use App\Http\Requests;
use Firebase\JWT\JWT;
use Carbon\Carbon;
use App\User;
use DB;

class PublicController extends Controller
{
    use TokenId;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $req)
    {
        //
    }

    public function muchNotif(Notification $Notif,Request $req){
        $user_id = $this->GTI($req);

        $query = DB::table('sys_pesan')->where('via_android', 1)->where('flg_sent', 0)->limit(1)->first();

        $to = $query->fcm_token;
        // $fcm_token = 'eqcn7zfBA40:APA91bH7yuHWU_K_top5vRMVCI5uqH1dURhg3cpaysX3vMeXKpAZYa_GB_RrpylFeXlaCcY2COPXTi0Uvi_loz6K3ypXHSYqbKgoAMgzR_KgmBmwh2_AAzJL4fxEJlI3Q2AIpQuHotv8';


        if($query != null || $query != ''){
            $to = $fcm_token;
            $title = $query->subject;
            $message = $query->pesan;

            $inData = $Notif->curl_post($to, $title, $message);
            $outData = json_decode($inData, true);

            $xData = $outData['success'];

            if ($xData == 1) {
                $queryUpdate = DB::update("UPDATE sys_pesan SET flg_sent = 1 WHERE `user_id`=?",[$user_id]);

                return response()->json([
                    'status'=>'success',
                    'message'=> 'Notification has been send'
                ], 200);
            }else{
                return response()->json([
                    'status'=>'error',
                    'message'=> 'failed to send notification, check your connection'
                ], 400);
            }
        }else{
            return response([
                'status' => 'Bad Request',
                'data' => 'Data not found'
            ], 400);
        }
    }

    public function oneNotif(Notification $Notif,Request $req){
        $to = $req->fcm_token;
        $title = $req->title;
        $message = $req->message;

        try {
            $inData = $Notif->curl_post($to, $title, $message);
            $outData = json_decode($inData, true);
        } catch (Exception $e) {
            return response()->json([
                'message' => 'something wrong'
            ], 401);
        }

        return response()->json([
            'message' => 'was sent'
        ], 200);
    }

    public function aboutUs(){
        try {
            $query = DB::table('kmi_visi_misi')->first();
        }catch(\Exception $e){
			return response()->json([
	       		'status'=>'error',
	       		'message'=>$e
			], 412);
		}

        return response([
            [
                "header" => "About Us",
                "value" => $query->about
            ],
            [
                "header" => "Visi",
                "value" => $query->visi
            ],
            [
                "header" => "Misi",
                "value" => $query->misi
            ],
        ], 200);
    }

    public function banner(){
        try {
            $query = DB::select('SELECT * FROM kmi_banner WHERE `show` = 1 AND CURDATE() < expired_date');

        }catch(\Exception $e){
			return response()->json([
	       		'status'=>'error',
	       		'message'=>$e
			], 412);
		}

        return response(
         $query
        , 200);
    }

    public function news(){
        try {
            $query = DB::select("SELECT id, title, link, short_content, concat('<img src=\"', link ,'\" width=\"375\" height=\"204\">', ' ', content) as content, `show`, ".
            "DATE_FORMAT(create_date, '%Y-%m-%d %h:%m:%s') AS create_date , ".
            "DATE_FORMAT(publish_date,'%Y-%m-%d %h:%m:%s') AS publish_date, ".
            "DATE_FORMAT(expired_date,'%Y-%m-%d %h:%m:%s') AS expired_date ".
            "FROM kmi_news ".
            "WHERE `show` = 1 AND curdate() < expired_date ".
            "ORDER BY publish_date DESC");

        }catch(\Exception $e){
			return response()->json([
	       		'status'=>'error',
	       		'message'=>$e
			], 412);
		}

        return response(
            $query
        ,200);
    }

    public function otorisasi(Request $req){

        $user_id = $this->GTI($req);

        try {
            $query = DB::select("SELECT id, `subject` AS title, NULL AS link, `subject` AS short_content, `pesan` AS content, otorisasi, ".
            "CONCAT_WS(' ',DATE_FORMAT(`tgl`, '%Y-%m-%d'), DATE_FORMAT(`jam`, '%h:%m:%s')) AS create_date , ".
            "CONCAT_WS(' ',DATE_FORMAT(`tgl`, '%Y-%m-%d'), DATE_FORMAT(`jam`, '%h:%m:%s')) AS publish_date, ".
            "CONCAT_WS(' ',DATE_FORMAT(`tgl`, '%Y-%m-%d'), DATE_FORMAT(`jam`, '%h:%m:%s')) AS expired_date ".
            "FROM `flg_otorisasi` ".
            "WHERE `subject` IS NOT NULL AND otorisasi=0 AND `user_id`=? ".
            "ORDER BY id LIMIT 15", [$user_id]);

        }catch(\Exception $e){
            return response()->json([
                'status'=>'error',
                'message'=>$e
            ], 412);
        }

        return response([
            'status' => 'success',
            'data' => $query
        ], 200);
    }

    public function historisasi(Request $req){
        $user_id = $this->GTI($req);
        try {
            $query = DB::select("SELECT id, `subject` AS title, NULL AS link, `subject` AS short_content, `pesan` AS content, otorisasi, ".
            "CONCAT_WS(' ',DATE_FORMAT(`tgl`, '%Y-%m-%d'), DATE_FORMAT(`jam`, '%h:%m:%s')) AS create_date , ".
            "CONCAT_WS(' ',DATE_FORMAT(`tgl`, '%Y-%m-%d'), DATE_FORMAT(`jam`, '%h:%m:%s')) AS publish_date, ".
            "CONCAT_WS(' ',DATE_FORMAT(`tgl`, '%Y-%m-%d'), DATE_FORMAT(`jam`, '%h:%m:%s')) AS expired_date ".
            "FROM `flg_otorisasi` ".
            "WHERE `subject` IS NOT NULL AND otorisasi<>0 AND `user_id`=? ".
            "ORDER BY id DESC LIMIT 15", [$user_id]);
        }catch(\Exception $e){
			return response()->json([
	       		'status'=>'error',
	       		'message'=>$e
			], 412);
		}

        return response([
			'status' => 'success',
			'data' => $query
		], 200);
    }

    public function assetJual(){
        try {
            $query = DB::select('SELECT * FROM kmi_jual_aset WHERE `show` = 1 ORDER BY create_date DESC');
            $data = array();
            $i = 0;

            foreach ($query as $key => $val) {
                $data[$i] = [
                    'id'            => (int) $val->id,
                    'title'         => $val->title,
                    'link'          => $val->link,
                    'short_content' => $val->short_content,
                    'content'       => $val->content,
                    'show'          => (int) $val->show,
                    'create_date'   => $val->create_date,
                    'expired_date'  => $val->expired_date,
                    'last_update'   => $val->last_update,
                ];
                $i++;
            }
        }catch(\Exception $e){
			return response()->json([
	       		'status'=>'error',
	       		'message'=>$e
			], 412);
		}

        return response($data, 200);
    }

    public function product(){
        try {

            $query = DB::select("SELECT concat('<img src=\"', link ,'\" width=\"375\" height=\"204\">', ' ', content) as content, `id`, `title`, `link`, `short_content`, `show`, `create_date`, `expired_date`, `last_update` FROM kmi_produk WHERE `show` = 1");

            $i = 0;
            foreach ($query as $key => $val) {
                $data[$i++] = [
                    'content' => $val->content,
                    'id' => (int) $val->id,
                    'title' => $val->title,
                    'link' => $val->link,
                    'short_content' => $val->short_content,
                    'show' => (int) $val->show,
                    'create_date' => $val->create_date,
                    'expired_date' => $val->expired_date,
                    'last_update' => $val->last_update
                ];
            }

        }catch(\Exception $e){
			return response()->json([
	       		'status'=>'error',
	       		'message'=>$e
			], 412);
		}

        return response($data, 200);
    }

    public function location(){
        try {
            $query = DB::select("SELECT * FROM kmi_lokasi");
        }catch(\Exception $e){
			return response()->json([
	       		'status'=>'error',
	       		'message'=>$e
			], 412);
		}

        return response($query, 200);
    }

    public function tenorKredit(){
        // $path = realpath('json/simulasikredit.json');

        // $json = json_decode(file_get_contents($path), true);

        $list = array(
            [
                 "nama" => "6 bulan",
                 "value" => 6
            ],
            [
                 "nama" => "12 bulan",
                 "value" => 12
            ],
            [
                 "nama" => "18 bulan",
                 "value" => 18
            ],
            [
                 "nama" => "24 bulan",
                 "value" => 24
            ],
            [
                 "nama" => "30 bulan",
                 "value" => 30
            ],
            [
                 "nama" => "36 bulan",
                 "value" => 36
            ],
            [
                 "nama" => "40 bulan",
                 "value" => 40
            ],
            [
                 "nama" => "48 bulan",
                 "value" => 48
            ]
        );

        return response($list, 200);
    }

    public function kodeKantor(){
        try {
            $query = DB::select('SELECT kode_kantor, nama_area_kerja as nama_kantor, nama_area_kerja FROM app_kode_kantor WHERE  kode_kantor NOT IN(31,32)');
        }catch(\Exception $e){
			return response()->json([
	       		'status'=>'error',
	       		'message'=>$e
			], 412);
		}

        return response($query, 200);
    }
}
