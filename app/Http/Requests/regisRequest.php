<?php

namespace App\Http\Requests;

use Pearl\RequestValidate\RequestAbstract;

class regisRequest extends RequestAbstract
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            "fullname"          => "required",
            "nasabah_id"        => "numeric",
            "alamat"            => "required",
            "no_ktp"            => "required|numeric|unique:kmi_user",
            "no_hp"             => "required|numeric|unique:kmi_user|digits_between:10,13",
            "email"             => "required|email|unique:kmi_user",
        ];
    }

    public function messages(): array
    {
        return [
            "fullname.required"       => "Fullname tidak boleh kosong",
            "nasabah_id.numeric"      => "Nasabah_id harus berupa angka",
            "kode_kantor.required"    => "kode_kantor tidak boleh kosong",
            "alamat"                  => "Alamat tidak boleh kosong",
            "no_ktp.required"         => "no_ktp tidak boleh kosong",
            "no_ktp.numeric"          => "no_ktp harus berupa angka",
            "no_ktp.unique"           => "no_ktp sudah ada yang menggunakan",
            "no_hp.required"          => "no_hp tidak boleh kosong",
            "no_hp.numeric"           => "no_hp harus berupa angka",
            "no_hp.unique"            => "no_hp sudah ada yang menggunakan",
            "no_hp.digits_between"    => "no_hp harus berjumlah antara 10 - 13 digit angka",
            "email.required"          => "Email tidak boleh kosong",
            "email.email"             => "Format email tidak benar",
            "email.unique"            => "Email sudah ada yang menggunakan",
        ];
    }
}
