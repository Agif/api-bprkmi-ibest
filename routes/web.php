<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function(){
    return redirect('api');
});

$router->get('user/verify', 'UserController@verify');

$router->post('email', 'Email@send');


$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['namespace' => 'App\Http\Controllers'], function ($api) {
    //tempat end-point yang akan didefinisikan
    $api->get('/', function(){
        echo "This is API KMI-Ibest EndPoint for User";
    });

    $api->post('user/login', 'UserController@login');
    $api->post('user/create', 'UserController@create');

    $api->group(['prefix' => '/public', 'namespace' => 'Publics'], function ($api){
        $api->post('send-notif/{actions}', 'PublicController@muchNotif'); //Web Socket Email
        $api->post('send-notifikasi', 'PublicController@oneNotif'); //Push Notif to Device

        $api->get('about_us', 'PublicController@aboutUs');
        $api->get('banner', 'PublicController@banner');
        $api->get('news', 'PublicController@news');
        $api->get('otorisasi', 'PublicController@otorisasi');
        $api->get('history-otorisasi', 'PublicController@historisasi');
        $api->get('asset', 'PublicController@assetJual');
        $api->get('product', 'PublicController@product');
        $api->get('location', 'PublicController@location');
        $api->get('tenor-kredit', 'PublicController@tenorKredit');
        $api->get('kode-kantor', 'PublicController@kodeKantor');
    });
});

$api->version('v1',['middleware' => 'jwt.auth', 'namespace' => 'App\Http\Controllers'], function ($api){

    $api->group(['prefix' => '/user'], function ($api){
        $api->get('send-verification/{email}', 'UserController@emailVerification');
    });

    $api->group(['prefix' => '/private', 'namespace' => 'Privates'], function ($api){
        $api->get('simulasi-kredit/{plafon}/{tenort}', 'PrivateController@simulasiKredit'); //Belum
        $api->get('notif', 'PrivateController@notif');
        $api->get('tracking-order/{no_aplikasi}', 'PrivateController@tracking_order');
        $api->get('count-notif', 'PrivateController@countNotif');
        $api->post('update-notif/{id_notification}', 'PrivateController@updateNotif');

        $api->get('test', 'PrivateController@test');

        $api->get('update-otorisasi/{id_otorisasi}/{flg_otorisasi}', 'PrivateController@flg_oto');
        $api->get('get-data-user', 'PrivateController@get_data_user');

        $api->get('simpanan', 'PrivateController@simpanan');
        $api->get('saldo-deposit/{no_rekening}/{tanggal}', 'PrivateController@saDeposit');
        $api->get('details-transaksi-deposito/{no_rekening}/{tanggalStart}/{tanggalEnd}', 'PrivateController@detailTransaksi');
        $api->get('saldo-tabungan/{no_rekening}/{tanggalTrans}', 'PrivateController@saldoTabungan');
        $api->get('details-transaksi-tabungan/{no_rekening}/{tanggalStart}/{tanggalEnd}', 'PrivateController@dtt');
        $api->get('kredit', 'PrivateController@kredit');
        $api->get('riwayat-kredit/{no_rekening}', 'PrivateController@riwayatKredit');

        $api->post('upload/avatar', 'PrivateController@up_avatar');
        $api->get('avatar', 'PrivateController@avatar');
        $api->post('user/edit-profile', 'PrivateController@editProfile');
        $api->get('sms-token', 'PrivateController@smsToken');
        $api->get('change-phone/{no_hp}', 'PrivateController@changePhone');
        $api->get('verify-nohp/{otp_kode}', 'PrivateController@otp_kode');
        $api->get('ajukankredit', 'PrivateController@ajukankredit');
    });

    $api->get('private/signout', 'UserController@logout');

    //$api->delete('delete/{id}', 'UserController@delete'); //Delete User
});
