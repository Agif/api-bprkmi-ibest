<?php

namespace App\Http\Requests;

use Pearl\RequestValidate\RequestAbstract;

class uploadImageRequest extends RequestAbstract
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'avatar_user' => 'mimes:jpeg,jpg,png|max:2000'
        ];
    }

    public function messages(): array
    {
        return [
            // "avatar_user.required" => ":attribute belum dipilih",
            "avatar_user.mimes"    => ":attribute Harus berformat gambar dengan ekstensi jpeg, jpg atau png",
            "avatar_user.max"      => ":attribute maksimal berukuran 2 Mb"
        ];
    }
}
