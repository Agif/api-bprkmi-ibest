<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\regisRequest;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Traits\TokenId;
use App\Http\Requests;
use Firebase\JWT\JWT;
use Carbon\Carbon;
use App\User;
use DB;

use Illuminate\Support\Facades\Mail;


class UserController extends Controller
{
    use TokenId;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $request;

    protected function jwt(User $user) {
         $payload = [
            //'iss'      => "lumen-jwt", // Issuer of the token
            'id'         => $user->id,
            'nasabah_id' => $user->nasabah_id,
            'no_hp'      => $user->no_hp,
            'iat'        => time() // Time when JWT was issued.
            //'exp'        => time() + 60*60*7 // Expiration time
            //'exp'        => time() + 10 // Expiration time
        ];
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function login(User $user, Request $request) {
        $reqEmail = $request->input('email');
        $reqImei  = $request->input('imei');
        $reqFcm   = $request->input('fcm_token');

        if (!$reqEmail) {
            return response()->json([
                "code"    => "404",
                "status"  => "bad request",
                "message" => "Email Tidak Boleh Kosong"
            ], 404);
        }

        $user = User::where('email', $reqEmail)->first();

        if (!$user) {
            return response()->json([
                "code"    => "404",
                "status"  => "Not Found",
                "message" => "Email ".$reqEmail." Belum Terdaftar"
            ], 404);
        }

        if ($user->user_verified == '2') {
            return response()->json([
                "code"    => "400",
                "status"  => "bad request",
                "message" => "Maaf, akun anda telah di blokir"
            ], 400);
        }

        if($user->imei && $user->imei != $request->imei){
            return response()->json([
                "code"    => "400",
                "status"  => "bad request",
                "message" => "Anda telah login di device lain"
            ], 400);
        }

        if ($user->email == $reqEmail && $user->email_verified == '1'){

            DB::update('UPDATE kmi_user SET `imei`=?, fcm_token=? WHERE `email`=? ',[$reqImei, $reqFcm, $user->email]);

            return response()->json([
                'message'=>'Success Login',
                'token'=> $this->jwt($user)
            ], 200);
        }else{
            return response()->json([
                "code"    => "400",
                "status"  => "bad request",
                "message" => "please verify email"
            ], 400);
        }
    }

    public function create(Request $req) {

        if(!preg_match("/^[a-zA-Z ]*$/", $req->input('fullname'))) {
            return response()->json([
                "code"    => "404",
                "status"  => "bad request",
                "message" => " Nama tidak boleh berupa angka"
            ], 404);
        }

        if(!preg_match("/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,3})$/", $req->input('email'))) {
            return response()->json([
                "code"    => "400",
                "status"  => "bad request",
                "message" => "Email tidak valid"
            ], 404);
        }

        if(!preg_match("/^([0-9]{16})$/", $req->input('no_ktp'))) {
            return response()->json([
                "code"    => "400",
                "status"  => "bad request",
                "message" => " No KTP harus berupa angka dan berjumlah 16 digit"
            ], 400);
        }

        if(!preg_match("/^(^\+62\s?|^08)(\d{3,4}-?){2}\d{3,3}$/", $req->input('no_hp'))) {
            return response()->json([
                "code"    => "400",
                "status"  => "bad request",
                "message" => " Awali nomor hp anda dengan '08' dan jumlah angka antara '11 - 13' digit"
            ], 400);
        }

        $checkKTP = User::where('no_ktp', $req->input('no_ktp'))->first();
        $checkEMAIL = User::where('email', $req->input('email'))->first();
        $checkHP = User::where('no_hp', $req->input('no_hp'))->first();

        if ($checkKTP != null) {
            return response()->json([
                'status' => 'Error',
                'message' => 'No KTP sudah terdaftar'
            ], 400);
        }

        if ($checkEMAIL != null) {
            return response()->json([
                'status' => 'Error',
                'message' => 'Email sudah terdaftar'
            ], 400);
        }

        if ($checkHP != null) {
            return response()->json([
                'status' => 'Error',
                'message' => 'No HP sudah terdaftar'
            ], 400);
        }

        $strRandom = Str::random(10);
        $verified_idx = "kmiuser".$strRandom.$req->input('email');
        $timeNow = Carbon::now()->toDateTimeString(); // Produces something like "2019-03-11 12:25:00"
        $link    = URL('user/verify?id='.$verified_idx);//Or this ....... $host = URL::to('/');

        DB::beginTransaction();

        $data = new User;
        $data->fullname          = $req->input('fullname');
        $data->kode_kantor       = $req->input('kode_kantor');
        $data->link              = $link;
        $data->alamat            = $req->input('alamat');
        $data->no_ktp            = $req->input('no_ktp');
        $data->no_hp             = $req->input('no_hp');
        $data->email             = $req->input('email');
        $data->email_verified_id = $verified_idx;
        $data->create_date       = $timeNow;
        $data->save();

        if ($req->hasFile('avatar_user')) {
            $file = $req->file('avatar_user');
            $path = base_path('public/images/uploaded_images/');
            $name = $data->id.'avatarprofile.'.$file->getClientOriginalExtension();
            $file->move($path, $name);

            $data->avatar_user = 'public/images/uploaded_images/'.$name;
        }

        if ($data->save()) {

            $view    = 'email.emailTemplate';
            $from    = 'confirm@kreditmandiri.co.id';
            $to      = $req->email;
            $cc      = false;
            $subject = 'Please confirm your KMI User account';

            $files = $req->file('attach');

            $msg = array(
                'nameBody' => $req->fullname,
                'linkBody' => $link
            );

            Mail::send(['html' => $view], $msg, function($message) use($from, $subject, $to, $cc, $files){
                $message->to($to);
        
                if ($cc) {
                    $message->cc($cc);
                }
        
                if($files != null ){
                    foreach($files as $file){
                        $message->attach($file->getRealPath(),
                        [
                            'as' => $file->getClientOriginalName(),
                            'mime' => $file->getClientMimeType(),
                        ]);
                    }
                }
        
                if ($from) {
                    $message->from($from);
                }
        
                $message->subject($subject);
            });

            if (Mail::failures()) {
                // return response showing failed emails
                DB::rollback();
                return response([
                    'status'  => 'Error',
                    'message' => 'Gagal Mengirim Email'
                ], 400);
        
            }else{
                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' => 'Please check your email inbox or spam folder to verify account KMI User'
                ], 201);
            }

        }else{
            DB::rollback();
            return response([
                'status'  => 'Error',
                'message' => 'Gagal registrasi'
            ], 400);
        }
    }

    public function emailVerification($email, Request $req){
        $strRandom    = Str::random(10);
        $verified_idx = "kmiuser".$strRandom.$email;
        $link         = URL("/user/verify?id=".$verified_idx);

        // $queryShow = DB::select('SELECT fullname FROM kmi_user WHERE `email`= ? ', ['oka']);
        $queryShow = User::where('email', $email)->first();

        if (!$queryShow) {
            return response()->json([
                'status'  => 'error',
                'message' => 'Email Tidak terdaftar'
            ], 412);
        }

        $query = User::where('email', $queryShow->email)->update([
            'email_verified_id' => $verified_idx
        ]);

        if ($query) {
            $view    = 'email.emailTemplate';
            $from    = false; // 'noreply@kreditmandiri.co.id'
            $to      = 'aplikasi@kreditmandiri.co.id';
            $cc      = false;
            $subject = 'Please confirm your KMI User account';

            $files = array();
            for ($x = 1; $x <= 5; $x++) {
                $files[$x] = $req->file("attach$x");
            }

            $msg = array(
                'nameBody' => $queryShow->fullname,
                'linkBody' => $link
            );

            $send = $this->mail($view, $from, $to, $cc, $subject, $msg, $files);

            try {
                return response()->json([
                    "message" => "Please check your email to verify account KMI IBEST"
                ], 201);
            } catch (Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'something wrong please try again later'
                ], 404);
            }
        }else {
            return response()->json([
                'status' => 'error',
                'message' => 'Your email not ready'
            ], 412);
        }
    }

    public function verify(Request $req){
        $veriGet = DB::table('kmi_user')->where('email_verified_id', $req->id)->where('email_verified', 0)->first();

        if ($veriGet == null) {
            return view('redirect.expired');
        }

        $veriUp = DB::table('kmi_user')->where('email_verified_id', $req->id)->update(['email_verified' => 1]);

        if ($veriUp == 1) {
            return view('redirect.success');
        }else{
            return view('redirect.expired');
        }
    }

    public function logout(Request $req){

        $user_id = $this->GTI($req);

        DB::table('kmi_user')->where('id', $user_id)->update(['imei' => '']);

        return response()->json([
            'message' => 'Berhasil Sign out'
        ], 201);
    }

    public function delete($id){
        try{
            $data = User::find($id);
            $data->delete();
        }catch(\Exception $e){
			return response()->json([
	       		'status'=>'error',
	       		'message'=>$e
			], 412);
		}

		return response()->json([
			'status' => 'success',
			'message' => 'Data dengan id = '.$id.' berhasil dihapus'
		], 200);
    }
}
