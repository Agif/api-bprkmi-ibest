<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Requests;

class Email extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function send(Request $req)
    {
        $view    = 'email.isiemail';
        $from    = 'noreply@kreditmandiri.co.id';

        $to = $req->input('tujuan');
        $cc = $req->input('cc');

        $subject = $req->input('subyek');

        $files = array();
        for ($x = 1; $x <= 5; $x++) {
            $files[$x] = $req->file("attach$x");
        }

        $msg = array(
            'pesan' => $req->input('pesan')
        );

        $send = $this->mail($view, $from, $to, $cc, $subject, $msg, $files);

        try {
            return response()->json([
                'status' => 'success',
                'message'=> 'email berhasil dikirim'
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'status' => 'error',
                'message'=> 'gagal mengirim email'
            ], 412);
        }
    }
}
